import 'package:cartography_app/data/models/directions_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class LocationService {
  static const String apiKey = 'AIzaSyAEyLzNRloNBckHtMJThGe1VoKPfnlMbM8';
  static const String baseUrl = 'https://maps.googleapis.com/maps/api';

  Future<String> getPlaceId(String input) async {
    final String url = '$baseUrl/place/findplacefromtext/json?input=$input&inputtype=textquery&key=$apiKey';

    var responce = await http.get(Uri.parse(url));
    var json = convert.jsonDecode(responce.body);
    var placeId = json['candidates'][0]['place_id'] as String;

    return placeId;
  }

  Future<Map<String, dynamic>> getPlace(String input) async {
    final placeId = await getPlaceId(input);

    final String url = '$baseUrl/place/details/json?place_id=$placeId&key=$apiKey';

    var responce = await http.get(Uri.parse(url));
    var json = convert.jsonDecode(responce.body);
    var results = json['result'] as Map<String, dynamic>;

    return results;
  }

  Future<DirectionsModel> getDirections(LatLng origin, LatLng destination) async {
    const String url = '$baseUrl/directions/json?';

    final Map<String, dynamic> queryParams = {
      'origin': '${origin.latitude},${origin.longitude}',
      'destination': '${destination.latitude},${destination.longitude}',
      'key': apiKey,
    };

    final Uri uri = Uri.parse(url);
    final Uri uriWithQueryParams = uri.replace(queryParameters: queryParams);
    final response = await http.get(uriWithQueryParams);
    var json = await convert.jsonDecode(response.body);
    final directionsModel = DirectionsModel.fromMap(json);
    
    return directionsModel;
  }
}
