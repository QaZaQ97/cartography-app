import 'package:flutter/material.dart';

class SearchBarWidget extends StatelessWidget {
  final TextEditingController controller;
  final VoidCallback onPress;

  const SearchBarWidget({
    super.key,
    required this.controller,
    required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            controller: controller,
            textCapitalization: TextCapitalization.words,
            decoration: const InputDecoration(hintText: 'Введите адрес'),
          ),
        ),
        IconButton(
          onPressed: onPress,
          icon: const Icon(Icons.search),
        ),
      ],
    );
  }
}
