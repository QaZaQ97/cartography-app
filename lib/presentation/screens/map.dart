import 'dart:async';
import 'package:cartography_app/data/models/directions_model.dart';
import 'package:cartography_app/data/services/location_service.dart';
import 'package:cartography_app/presentation/screens/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Completer<GoogleMapController> mapController = Completer<GoogleMapController>();
  TextEditingController searchController = TextEditingController();

  DirectionsModel? directions;
  LatLng? _currentPosition;
  LatLng? _destinationPosition;
  bool _isLoading = true;

  Set<Marker> markers = <Marker>{};
  Set<Polyline> polylines = <Polyline>{};

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  Future<void> _getLocation() async {
    final permission = await Geolocator.requestPermission();
    if (permission != LocationPermission.denied) {
      final position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );
      final lat = position.latitude;
      final long = position.longitude;

      final location = LatLng(lat, long);

      setState(() {
        _currentPosition = location;
        _isLoading = false;
        _addMarker(location);
      });
    }
  }

  void _addMarker(LatLng point) {
    setState(() {
      markers.add(
        Marker(
          markerId: const MarkerId('marker'),
          position: point,
        ),
      );
    });
  }

  Future<void> _goToPlace(Map<String, dynamic> place) async {
    final lat = place['geometry']['location']['lat'];
    final lng = place['geometry']['location']['lng'];

    final GoogleMapController controller = await mapController.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, lng), zoom: 16),
      ),
    );
    setState(() {
      _destinationPosition = LatLng(lat, lng);
    });
    _addMarker(LatLng(lat, lng));
  }

  Future<void> _getDirections() async {
    final directionsInfo = await LocationService().getDirections(
      _currentPosition!,
      _destinationPosition!,
    );
    setState(() {
      directions = directionsInfo;

      if (directions != null) {
        final polylinePoints = directions!.polylinePoints.map((e) => LatLng(e.latitude, e.longitude)).toList();

        const PolylineId id = PolylineId('polyline');
        final Polyline polyline = Polyline(
          polylineId: id,
          color: Colors.red,
          width: 4,
          points: polylinePoints,
        );

        polylines.add(polyline);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Картография')),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : Column(
              children: [
                SearchBarWidget(
                  controller: searchController,
                  onPress: () async {
                    var place = await LocationService().getPlace(searchController.text);
                    _goToPlace(place);
                  },
                ),
                Text('${directions?.totalDistance ?? ''} ${directions?.totalDuration ?? ''}'),
                Expanded(
                  child: GoogleMap(
                    onMapCreated: (GoogleMapController controller) {
                      mapController.complete(controller);
                    },
                    polylines: polylines,
                    myLocationEnabled: true,
                    markers: markers,
                    initialCameraPosition: CameraPosition(
                      target: _currentPosition ?? const LatLng(0.0, 0.0),
                      zoom: 16.0,
                    ),
                  ),
                ),
                if (_currentPosition != null && _destinationPosition != null)
                  FloatingActionButton(
                    child: const Icon(Icons.directions_walk),
                    onPressed: () => _getDirections(),
                  )
                else
                  const SizedBox(),
              ],
            ),
    );
  }
}
